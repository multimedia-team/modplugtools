modplugtools (0.5.6-3) unstable; urgency=medium

  * Bump standards version to 4.7.0.
  * d/control: update build depends pkg-config to pkgconf.

 -- Alex Myczko <tar@debian.org>  Mon, 15 Jul 2024 21:09:19 +0000

modplugtools (0.5.6-2) unstable; urgency=medium

  * d/watch: fix copy paste error.
  * d/clean: updated.

 -- Gürkan Myczko <tar@debian.org>  Mon, 17 Jan 2022 11:33:45 +0100

modplugtools (0.5.6-1) unstable; urgency=medium

  * New upstream version.
  * d/control:
    - bump debhelper version to 13, drop d/compat.
    - bump standards version to 4.6.0.
    - add build-depends: cmake.
    - add to debian multimedia team.
    - add Vcs fields.
  * Update maintainer address.
  * d/rules:
    - update copyright years.
    - set upstream-name.
  * d/upstream: added.
  * d/watch: update URL and version.
  * d/*.1: dropped and using upstream manpage, drop d/manpages.
  * d/patches: dropped.
  * d/docs: updated.

 -- Gürkan Myczko <tar@debian.org>  Tue, 11 Jan 2022 16:50:59 +0100

modplugtools (0.5.3-3) unstable; urgency=medium

  * Bump debhelper version to 11.
  * Bump standards version to 4.2.1.
  * debian/control: Replace libmodplug-dev with libopenmpt-modplug-dev.
  * debian/rules: Drop autotools call.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Sep 2018 15:33:40 +0200

modplugtools (0.5.3-2) unstable; urgency=medium

  * Bump debhelper version to 10.
  * Bump standards version to 4.0.0.
  * Reference shuf instead of rl. (Closes: #801217)
  * Update my name.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 16 Sep 2017 11:25:25 +0200

modplugtools (0.5.3-1.1) unstable; urgency=low

  * NMU
  * run dh-autoreconf to update config.{sub,guess} and
    {libtool,aclocal}.m4. Closes: #727462

 -- Steve McIntyre <93sam@debian.org>  Tue, 14 Oct 2014 22:00:29 +0100

modplugtools (0.5.3-1) unstable; urgency=low

  * New upstream version. (Closes: #622703)
  * Bump debhelper version to 8.
  * Bump standards version to 3.9.2.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 14 Apr 2011 13:02:46 +0200

modplugtools (0.5.2-1) unstable; urgency=low

  * Initial release. (Closes: #513077)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 21 Feb 2011 11:11:09 +0100
